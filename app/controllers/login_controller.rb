class LoginController < ApplicationController
  class << self
    def url
      '/login'
    end
  end

  def new
  end

  def create
    permitted = login_params
    if !login(permitted[:username], permitted[:password])
      flash[:invalid_login] = true
      redirect_to :new
    else
      redirect_to permitted[:redirect_url]
    end
  end

  private

  def login_params
    params.require(:login).permit(:username, :password, :redirect_url)
  end

  def login(username, password)
    user = User.authenticate(username, password)
    user_nil = user.nil?
    session[:current_user_id] = user.id unless user_nil
    session = Session.reset_session session
    !user_nil
  end
end
