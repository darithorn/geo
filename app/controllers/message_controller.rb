class MessageController < ApplicationController
  def index
    if !Session.logged_in? session
      flash[:not_logged_in] = 'You\'re not logged in!'
      @messages = []
    else
      page = params[:page] || 0
      @messages = Message.find_by_from session[:current_user_id], page
    end
  end

  def show
    if !Session.logged_in? session
      flash[:not_logged_in] = 'You\'re not logged in!'
      @message = Message.new
    else
      @message = Message.find_by_id params[:id]
      if @message.nil?
        flash[:message_not_found] = 'That message wasn\'t found!'
        @message = Message.new
      elsif @message.from != session[:current_user_id] && @message.to != session[:current_user_id]
        flash[:no_access] = 'You cannot acess that message!'
        @message = Message.new
      end
  end

  def new
  end

  def create
    permitted = message_params
    user = User.find_by_username(permitted[:to])
    if !Session.logged_in? session
      flash[:not_logged_in] = 'You\'re not logged in!'
      redirect_to :new
    elsif user.nil?
      flash[:no_user_found] = "No user found by name #{permitted[:to]}"
      redirect_to :new
    else
      permitted[:from] = session[:current_user_id]
      message = Message.new(permitted)
      if message.save
        redirect_to message.url
      else
        redirect_to :new
      end
    end
  end

  private

  def message_params
    params.require(:message).permit(:to, :subject, :body)
  end
end
