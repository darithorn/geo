module ActivityFeed
  def self.group(group_ids)
    group_posts = {}
    group_ids.each do |id|
      group_posts[id] = GroupPost.get(id)
    end
  end
end
