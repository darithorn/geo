module Session
  def self.reset_session(session)
    current_user_id ||= session[:current_user_id]
    reset_session
    session[:current_user_id] ||= current_user_id
    session
  end

  def self.logged_in?(session)
    session.key?(:current_user_id) && !session[:current_user_id].nil?
  end
end
