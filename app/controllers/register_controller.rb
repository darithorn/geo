class RegisterController < ApplicationController
  def new
  end

  def create
    permitted = register_params
    valid_username = valid_username? permitted[:username]
    valid_password = valid_password? permitted[:password]
    valid_email_confirmation = permitted[:email] == permitted[:email_confirmation]
    valid = valid_username || valid_password || valid_email_confirmation
    if valid
      user = User.new(permitted)
      if user.save
        redirect_to LoginController.url, params: {
                                            login: {
                                                      username: permitted[:username],
                                                      password: permitted[:password],
                                                      redirect_url: request.referer
                                                    }
                                                  }
      end
    else
      flash[:invalid_username] = !valid_username
      flash[:invalid_password] = !valid_password
      flash[:invalid_email_confirmation] = !valid_email_confirmation
      redirect_to :new
    end
  end

  private

  def register_params
    params.require(:register).permit(:username,
                                     :password, :password_confirmation,
                                     :email, :email_confirmation)
  end

  def only_whitespace?(identifier)
    identifier.strip.length == 0
  end

  def valid_username?(username)
    username.strip.length > 3 &&
      username.strip.length < 20
  end

  def valid_password?(password)
    password.strip.length > 8 &&
      password.strip.length < 72
  end
end
