class Post < ActiveRecord::Base
  class << self
    def batch_size
      25
    end

    def find_latest_in_parent(parent, page = 0)
      Post.where("parent == #{parent}").order(:updated_at).offset(page * batch_size).limit(batch_size)
    end
  end

  def parent
    #return Event.find_by_id(parent) if event_post?
    #Group.find_by_id(parent)
  end

  def event_post?
    event
  end
end
