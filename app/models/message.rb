class Message < ActiveRecord::Base
  class << self
    def batch_size
      25
    end

    def find_by_from(id, page = 0)
      Message.where("from == #{id}").offset(page * batch_size).limit(batch_size)
    end

    def find_by_to(id, page = 0)
      Message.where("to == #{id}").offset(page * batch_size).limit(batch_size)
    end

    def find_by_to_from(to, from, page = 0)
      Message.where("to == #{to} && from == #{from}").offset(page * batch_size).limit(batch_size)
    end
  end

  def url
    "/message/#{id}"
  end

  def from
    User.find_by_id from
  end

  def to
    User.find_by_id to
  end

  def read?
    read
  end
end
