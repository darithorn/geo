require 'bcrypt'

# Schema Information
#
# Table: users
#
# id                        :integer      null: false, primary key
# name                      :string
# email                     :string       null: false
# email_confirmation_token  :string       null: false
# username                  :string       null: false
# password_digest           :string       null: false
# location                  :integer      default: nil
# confirmed_email           :boolean      default: false, null: false
# public_profile            :boolean      default: true, null: false
# admin                     :boolean      default: false, null: false
# created_at                :datetime
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_secure_password
  before_save :set_created_at

  def confirmed_email?
    confirmed_email
  end

  def public_profile?
    public_profile
  end

  def admin?
    admin
  end

  def location?
    !location.nil?
  end

  def location
    # Location.find_by_id(location)
  end

  private

  def set_created_at
    self.created_at = Date.today
  end
end
