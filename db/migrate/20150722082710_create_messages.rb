class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|

      t.timestamps null: false
      t.integer :from
      t.integer :to
      t.string :subject
      t.text :body
      t.boolean :read
      t.datetime :created_at
    end
  end
end
