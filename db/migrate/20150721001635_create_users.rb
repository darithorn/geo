class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.timestamps null: false
      t.string :name
      t.string :username, null: false
      t.string :password_digest, null: false
      # Location ID (locations table)
      t.integer :location, default: nil
      # Public Profile
      t.boolean :confirmed_email, default: false, null: false
      t.boolean :public_profile, default: true, null: false
      t.boolean :admin, default: false, null: false
      t.datetime :created_at
    end
  end
end
