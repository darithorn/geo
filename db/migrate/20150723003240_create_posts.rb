class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|

      t.timestamps null: false
      t.integer :parent, null: false
      t.boolean :event, default: false
      t.text :body, default: '', null: false
      t.datetime :created_at
    end
  end
end
