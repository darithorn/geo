Rails.application.routes.draw do
  devise_for :users
  root to: 'home#index'

  get '/'                   => 'home#index'
  get '/login'              => 'login#new'
  post '/login'             => 'login#create'
  get '/register'           => 'register#new'
  post '/register'          => 'register#create'

  get '/messages'           => 'message#index'
  get '/messages/:page'     => 'message#index'
  get '/message/:id'         => 'message#show'
  get '/message/new'        => 'message#new'
  post '/message/new'       => 'message#create'

  get '/u/:id'              => 'user#show'
  get '/u/:id/edit'         => 'user#new'
  post '/u/:id/edit'        => 'user#update'

  get '/g/create'           => 'group#new'
  post '/g/create'          => 'group#create'
  get '/g/:id'              => 'group#show'
  post '/g/:id/delete'      => 'group#destroy'
  #post '/g/:id/invite'      => 'group#invite'
  #post '/g/:id/mod'         => 'group#mod'

  get '/g/:id/post'         => 'grouppost#new'
  post '/g/:id/post'        => 'grouppost#create'
  get '/g/:id/:post'        => 'grouppost#show'
  get '/g/:id/:post/edit'   => 'grouppost#edit'
  post '/g/:id/:post/edit'  => 'grouppost#update'
  post '/g/:id/:post/delete'=> 'grouppost#destroy'

  get '/e/create'           => 'event#new'
  post '/e/create'          => 'event#create'
  get '/e/:id'              => 'event#show'
  post '/e/:id/delete'      => 'event#destroy'
  #post '/e/:id/invite'      => 'event#invite'
  #post '/e/:id/mod'         => 'event#mod'

  get '/e/:id/post'         => 'eventpost#new'
  post '/e/:id/post'        => 'eventpost#create'
  get '/e/:id/:post'        => 'eventpost#show'
  get '/e/:id/:post/edit'   => 'eventpost#edit'
  post '/e/:id/:post/edit'  => 'eventpost#update'
  post '/e/:id/:post/delete'=> 'eventpost#destroy'
end
